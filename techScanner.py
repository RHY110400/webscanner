import requests
import json

url = "http://testphp.vulnweb.com"
dir = "apps.json"

def get_html(url):    
        try:    
            html = requests.get(url, timeout=1)    
        except Exception as e:    
            print(e)    
            return ""    
        return html.content.decode('latin-1')  


def get_json(dir):
    with open(dir) as json_file:
        data = json.load(json_file)
        for a in data['apps']:
            for i in data['apps'][a]:
                print(i, " : ", data['apps'][a][i])

#print(get_html(url))
get_json(dir)