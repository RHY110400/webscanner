import sys
import urllib3
import getopt
import threading
import urllib.request
import queue
import requests    
import re    
from urllib.parse import urlparse  

# Variables used for input choices
ping = False
url = ""
brute_force = False
word_list_file = ""
threads = 5
spider = False
depth = 1
output_file = False
output_file_name = ""
spider_sites = []

# Variables outside of user input
user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/27.0.1453 Safari/537.36"
successful_dir_attempts = []


# Prints out how to use it and ends program
def usage():
    print()
    print("Website Scanner Tool")
    print()
    print("Usage: ")
    print(" -p --ping                         - ping website")
    print(" -u --url=[target_url]             - target url")
    print(" -t --threads=[No. of threads]     - set number of threads")
    print(" -b --bruteForce                   - brute force directories")
    print(" -w --wordlist                     - set wordlist to use")
    print(" -s --spider=[depth]               - spider website")
    print(" -o --output=[filename]            - output file")
    print(" -h --help                         - show help prompt")
    print()
    print("Examples: ")
    print("Python3 main.py -p -u [target_url]")
    print("Python3 main.py -b -t [threads] -w [wordlist dir] -u [target_url]")
    print("Python3 main.py -s [No. links to follow] -u [target_url]")
    print()
    print()
    sys.exit(0)




def main():
    # Grab user variables 
    global url
    global ping
    global brute_force
    global word_list_file
    global threads
    global spider
    global depth
    global output_file
    global output_file_name
    global spider_sites

    # If there are no inputs, show the usage
    if not len(sys.argv[1:]):
        usage()

    # Grab options and arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hpw:bu:t:s:o:", ["url=", "help", "ping", "bruteForce", "wordlist=", "threads", "spider=", "output="])
    except getopt.GetoptError as err:
        print(str(err))

    # Run through options and set user variables
    for o,a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-u", "--url"):
            url = a
        elif o in ("-p", "--ping"):
            ping = True
        elif o in ("-b", "--bruteForce"):
            brute_force = True
        elif o in ("-w", "--wordlist"):
            word_list_file = a
        elif o in ("-t", "--threads"):
            threads = int(a)
        elif o in ("-s", "--spider"):
            spider = True
            depth = int(a)
        elif o in ("-o", "--output"):
            output_file = True
            output_file_name = a


    # For all these options, run the relevant functions
    if ping:
        do_ping()

    if brute_force:
        run_dir_brute_forcer()
        #print(successful_dir_attempts)

    if spider:
        crawler = PyCrawler(url, depth)    
        crawler.start()

    if output_file:
        f = open(output_file_name + ".txt", 'w')
        for a in successful_dir_attempts:
            f.write(a + "\n")
        for a in spider_sites:
            f.write("\nLink: " + str(a[0]) + " \nDescription: "+ str(a[1]) + "\nKeywords: "+ str(a[2])+ "\n")
        f.close()

# Tests the connection and ends program if no connection
def do_ping():
    print("[*] Pinging website")
    try:
        http = urllib3.PoolManager()
        r = http.request('GET', url)
        print("Site returned with code:     ", r.status)
        if (r.status != 200):
            print("Exiting...")
            sys.exit(0)
    except:
        print("Did not Connect")
        print("Ending")
        sys.exit(0)


# Puts the wordlist into a queue object
def build_word_list(word_list_file):
    fd = open(word_list_file, "rb")
    word_list = fd.readlines()
    fd.close()

    if len(word_list):
        word_queue = queue.Queue()
        for word in word_list:
            word = word.rstrip().decode("utf8")
            word_queue.put(word)
        return word_queue
    return None

# Brute force web directories
def dir_brute_forcer(word_queue, target_url, extensions):
    while not word_queue.empty():
        attempt_list = []
        attempt = word_queue.get()

        if "." in attempt:
            attempt_list.append("/" + attempt)
            if extensions:
                for extension in extensions:
                    attempt_no_extension = attempt.split(".")[0]
                    attempt_extension = attempt_no_extension + extension
                    if attempt != attempt_extension:
                        attempt_list.append("/" + attempt_extension)
            else:
                attempt_list.append("/" + attempt)
        else:
            attempt_list.append("/" + attempt + "/")

        for brute in attempt_list:
            url = target_url + brute
            headers = {}
            headers["User-Agent"] = user_agent
            request = urllib.request.Request(url, headers=headers)
            try:
                response = urllib.request.urlopen(request)
                if len(response.read()):
                    if response.url not in successful_dir_attempts:
                        successful_dir_attempts.append(response.url)
                        print(f"[{response.code}] => {response.url}")
            except urllib.request.URLError as err:
                pass

# Runs the directory brute force with threads
def run_dir_brute_forcer():
    try:

        if word_list_file and url:
            word_queue = build_word_list(word_list_file)
            if word_queue:
                print(f"[*] Word Queue Created")
                print(f"[*] HTTP Brute Force Started")
                extensions = [".png", ".inc", ".php", ".orig"]
                #Since we are bruteforcing, we have multiple threads to speed it up
                #These threads will have access to the same word queue
                thread_list = []
                for i in range(threads):
                    brute_force_thread = threading.Thread(target=dir_brute_forcer, args=(word_queue, url, extensions))
                    thread_list.append(brute_force_thread)

                for x in thread_list:
                    x.start()
                for x in thread_list:
                    x.join()
                    
    except:
        usage()


# Spiders websites
# Class used as it has to store links
class PyCrawler(object):    
    def __init__(self, starting_url, links):    
        self.starting_url = starting_url      
        self.links = links                 
        self.visited = set()    

    # Get html of website
    def get_html(self, url):    
        try:    
            html = requests.get(url, timeout=1)    
        except Exception as e:    
            print(e)    
            return ""    
        return html.content.decode('latin-1')    

    # Find links in html without reference to self and add to links
    def get_links(self, url):    
        html = self.get_html(url)    
        parsed = urlparse(url)    
        base = f"{parsed.scheme}://{parsed.netloc}"    
        links = re.findall('''<a\s+(?:[^>]*?\s+)?href="([^"]*)"''', html)    
        for i, link in enumerate(links):    
            if not urlparse(link).netloc:    
                link_with_base = base + link    
                links[i] = link_with_base       

        return set(filter(lambda x: 'mailto' not in x, links))    

    # Get info on website
    def extract_info(self, url):    
        html = self.get_html(url)    
        meta = re.findall("<meta .*?name=[\"'](.*?)['\"].*?content=[\"'](.*?)['\"].*?>", html)    
        return dict(meta)    

    # Loop through links 
    def crawl(self, url):    
        for link in self.get_links(url):   
            if self.links < 0:
                break 
            self.links -= 1
            if link in self.visited:    
                continue    
            self.visited.add(link)    
            info = self.extract_info(link)    

            this_tuple = (link, info.get('description'), info.get('keywords'))
            spider_sites.append(this_tuple)

            

            print("Link: ", link)
            print("Description: ", info.get('disciprtion'))
            print("Keywords: ", info.get('keywords'))
            print()
      

    def start(self):
        print("[*] Starting spider")  
        print()           
        self.crawl(self.starting_url)

main()